:: Execute to delete old and make new counter

echo "Deleting old counter"
unlodctr PerfGen
echo "Deleting old register values"
reg delete HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\PerfGen\                     /f
reg delete HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\EventLog\Application\PerfGen /f
echo "Deleting old dll"
del "C:\Windows\System32\PerfGen.dll"
echo "Copying new dll"
copy "Z:\remote-console\PerfGen\x64\Debug\PerfGen.dll" "C:\Windows\System32\PerfGen.dll"
echo "Making new register values"
reg import "Z:\remote-console\PerfGen\Lab3\PerfGen.reg"
echo "Making new counter"
lodctr "Z:\remote-console\PerfGen\Lab3\PerfGen.ini"
