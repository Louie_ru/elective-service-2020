#include "header.h"

HANDLE g_ThreadStopEvent;

int _tmain(int argc, TCHAR *argv[]){
    g_ThreadStopEvent = CreateEvent(NULL, 0, 0, NULL);
    // Not needed for console
    SERVICE_TABLE_ENTRY ServiceTable[] = {{SERVICE_NAME, (LPSERVICE_MAIN_FUNCTION)ServiceMain},{NULL, NULL}};
    if (StartServiceCtrlDispatcher(ServiceTable) == FALSE){
        OutputDebugString(_T("StartServiceCtrlDispatcher error"));
        return GetLastError();
    }
    OutputDebugString(_T("Exit from Main"));
    return 0;
}


// Needed for console
//HANDLE hThread = CreateThread(NULL, 0, ServerThread, NULL, 0, NULL);
//CHAR buf;
//ReadFile(GetStdHandle(STD_INPUT_HANDLE), (LPVOID)&buf, 1, NULL, 0);